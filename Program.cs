﻿using System;
using System.Collections.Generic;
using System.Linq;

public class Test
{
	public static void Main()
	{
        RunTests();
	}
    
    /// Implement method which would order list of string
    ///  - by number of occurrences of the string (most frequent first),
    ///  - then by index of the first occurrence (first occurred first).
    private static IList<string> Sort(IList<string> inputArray)
    {
        // To be implemented.
        
    }
    
    private static void RunTests()
    {
        var testCases = new[]
        {
            (
                Input:        new[] { "foo", "bar", "bar" },
                Expected: new[] { "bar", "bar", "foo" }
            ),
            (
                Input:        new[] { "foo", "bar" },
                Expected: new[] { "foo", "bar" }
            ),
            (
                Input:        new[] { "foo", "bar", "foo", "bar", "hello" },
                Expected: new[] { "foo", "foo", "bar", "bar", "hello" }
            ),
            (
                Input:        new[] { "a", "b", "c", "1", "2", "2", "3", "3", "3", "b", "c", "c" },
                Expected: new[] { "c", "c", "c", "3", "3", "3", "b", "b", "2", "2", "a", "1" }
            ),
        };
        
        foreach (var testCase in testCases)
        {
            var actual = Sort(testCase.Input);
            var result = actual.SequenceEqual(testCase.Expected) ? "Ok!" : ":-(";
            Console.WriteLine($"{result}    -    {string.Join(" ", actual)}");
        }
    }
}


/*
using System;

namespace SampleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            // for name in BoxedClassTest BoxedStructTest NotBoxedClassTest NotBoxedStructTest RefClassTest RefStructTest ; do dotnet run 6000000 $name ; done
            TestRunner.Run(int.Parse(args[0]), args[1]);

            // FlowControl.FlowControlSample();
            
            // ClassStruct.ClassStructSample();
            
            // HelloName();

            Console.Read();
        }

        static void HelloName()
        {
            Console.WriteLine("What is your name?");
            var name = Console.ReadLine();
            Console.WriteLine($"Nice to meet you {name}!");
        }
    }
}
*/