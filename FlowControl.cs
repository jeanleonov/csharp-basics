using System;

namespace SampleApp
{
    static class FlowControl
    {
        public static void FlowControlSample()
        {
            int[] array = new int[4];
            int i;
      
            for (i = 0; i < array.Length; i++)
                array[i] = int.Parse(Console.ReadLine());

            i = 0;
            while (i < array.Length)
            {
                var elementInWhile = array[i];
                Console.WriteLine($"{i}: {elementInWhile}");
                i++;
            }

            int evenSumBefore99 = 0;
            for (i = 0; i < array.Length; i++)
            {
                int candidateForSum = array[i];
                if (candidateForSum == 99)
                    break;
                if (candidateForSum % 2 == 0)
                    evenSumBefore99 += candidateForSum;
            }
            Console.WriteLine(
                $"Sum of even elements " +
                $"before 99 = {evenSumBefore99}");
        }
    }
}